<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Managers;

use Generator;
use InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\IOException;
use Nette\NotSupportedException;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;
use Phar;
use PharData;
use ZipArchive;

use ElektroPotkan\Backups\Backup;
use ElektroPotkan\Backups\IBackup;
use ElektroPotkan\Backups\IJob;
use ElektroPotkan\Backups\ILogger;
use ElektroPotkan\Backups\IPurgeRule;


/**
 * Advanced backups manager creating an archive of the whole backup
 */
class Archive extends Locking {
	/** Available archive & compression formats */
	const
		ARCHIVE_FORMAT_AUTO = null,
		ARCHIVE_FORMAT_TAR = 'tar',
		ARCHIVE_FORMAT_TAR_BZIP2 = 'tar.bz2',
		ARCHIVE_FORMAT_TAR_GZIP = 'tar.gz',
		ARCHIVE_FORMAT_ZIP = 'zip';
	
	
	/** @var string */
	private $archiveName;
	
	/** @var bool */
	private $keepFiles;
	
	/** @var string */
	private $ext;
	
	
	/**
	 * Constructor
	 * @param string $archiveName - short name suffix
	 *   Must be different from all jobs' names (might be empty for no suffix).
	 *   Will be added to the file-name without any glue character
	 *   (use leading underscore to maintain the file-name format of standard backup files).
	 * @param bool $keepFiles
	 *   - true = keep also the individual backup files
	 *   - false = delete them after archive creation
	 */
	public function __construct(
		string $dir,
		string $name,
		IPurgeRule $purgeRule,
		?string $archiveFormat = self::ARCHIVE_FORMAT_AUTO,
		string $archiveName = '',
		bool $keepFiles = false
	){
		parent::__construct($dir, $name, $purgeRule);
		
		$this->archiveName = $archiveName;
		$this->keepFiles = $keepFiles;
		$this->ext = $this->selectFormat($archiveFormat);
	} // constructor
	
	/**
	 * Checks if required format is allowed and the necessary extensions are loaded
	 * Can automatically select format based on loaded extensions.
	 * @throws InvalidArgumentException when invalid format given
	 * @throws NotSupportedException when needed extension is not loaded
	 */
	protected function selectFormat(?string $format): string {
		if(extension_loaded('phar')){
			if(extension_loaded('bzip2')){
				if($format === self::ARCHIVE_FORMAT_AUTO || $format === self::ARCHIVE_FORMAT_TAR_BZIP2){
					return self::ARCHIVE_FORMAT_TAR_BZIP2;
				};
			}
			elseif($format === self::ARCHIVE_FORMAT_TAR_BZIP2){
				throw new NotSupportedException('Archive manager requires BZIP2 extension to work with the BZIP2 format but it is not loaded!');
			};
			
			if(extension_loaded('zlib')){
				if($format === self::ARCHIVE_FORMAT_AUTO || $format === self::ARCHIVE_FORMAT_TAR_GZIP){
					return self::ARCHIVE_FORMAT_TAR_GZIP;
				};
			}
			elseif($format === self::ARCHIVE_FORMAT_TAR_GZIP){
				throw new NotSupportedException('Archive manager requires ZLIB extension to work with the GZIP format but it is not loaded!');
			};
			
			if($format === self::ARCHIVE_FORMAT_AUTO || $format === self::ARCHIVE_FORMAT_TAR){
				return self::ARCHIVE_FORMAT_TAR;
			}
			elseif($format === self::ARCHIVE_FORMAT_ZIP){
				return self::ARCHIVE_FORMAT_ZIP;
			};
		}
		elseif(extension_loaded('zip')){
			if($format === self::ARCHIVE_FORMAT_AUTO || $format === self::ARCHIVE_FORMAT_ZIP){
				return self::ARCHIVE_FORMAT_ZIP;
			}
			elseif($format === self::ARCHIVE_FORMAT_TAR || $format === self::ARCHIVE_FORMAT_TAR_BZIP2 || $format === self::ARCHIVE_FORMAT_TAR_GZIP){
				throw new NotSupportedException('Archive manager requires PHAR extension to work with the TAR format but it is not loaded!');
			};
		}
		else {
			throw new NotSupportedException('Archive manager requires either PHAR or ZIP extension, neither is loaded!');
		};
		
		throw new InvalidArgumentException('Archive format should be one of the ARCHIVE_FORMAT_* class constants!');
	} // selectFormat
	
	public function addJob(string $name, IJob $job): void {
		if($this->archiveName !== '' && $this->archiveName.'.'.$this->ext === $this->formatJobFilename('', $name, $job)){
			throw new InvalidArgumentException('This job name cannot be used! Its resulting file-name collides with archive name (\''.$this->archiveName.'\').');
		};
		
		parent::addJob($name, $job);
	} // addJob
	
	protected function runJobsLocked(string $baseName): array {
		// let the grandparent Simple manager create all the backup files
		$files = Simple::runJobs($baseName);
		if(count($files) < 1){
			return $files;
		};
		
		// build archive name & path
		$this->log('Backups: Creating archive...');
		
		$archiveName = $baseName.$this->archiveName.'.';
		$archivePath = $this->getDir().'/'.$archiveName;
		
		$archivePathTmp = $archivePath . 'tmp';
		
		// create archive
		if(!extension_loaded('phar') && $this->ext === self::ARCHIVE_FORMAT_ZIP){
			$archive = new ZipArchive();
			if(($res = $archive->open($archivePathTmp, ZipArchive::CREATE | ZipArchive::OVERWRITE)) !== true){
				throw new IOException('ZipArchive - Failed to open archive!', $res);
			};
			
			try {
				foreach($files as $fn){
					if(!$archive->addFile($this->getDir().'/'.$fn, $fn)){
						throw new IOException('ZipArchive - Failed to add file!');
					};
				};
			}
			finally {
				$archive->close();
			};
		}
		else {
			$archive = new PharData(
				$archivePathTmp,
				PharData::CURRENT_AS_FILEINFO | PharData::KEY_AS_PATHNAME,
				'',
				(($this->ext === self::ARCHIVE_FORMAT_ZIP) ? Phar::ZIP : Phar::TAR)
			);
			
			$archive->buildFromIterator((function() use($files): Generator {
				foreach($files as $fn){
					yield $fn => $this->getDir().'/'.$fn;
				};
			})());
			
			if($this->ext === self::ARCHIVE_FORMAT_TAR_BZIP2){
				$archive->compress(Phar::BZ2, 'tmp.cmp');
				FileSystem::delete($archivePathTmp);
				$archivePathTmp .= '.cmp';
			}
			elseif($this->ext === self::ARCHIVE_FORMAT_TAR_GZIP){
				$archive->compress(Phar::GZ, 'tmp.cmp');
				FileSystem::delete($archivePathTmp);
				$archivePathTmp .= '.cmp';
			};
		};
		
		// rename to the final name
		$archiveName .= $this->ext;
		$archivePath .= $this->ext;
		
		FileSystem::rename($archivePathTmp, $archivePath);
		
		$this->log('Backups: Archive created.');
		
		// delete the other files if requested
		if($this->keepFiles){
			$files[] = $archiveName;
		}
		else {
			$this->log('Backups: Deleting other backup files...');
			
			foreach($files as $fn){
				$this->log("Backups: Deleting file '$fn'...", ILogger::DEBUG);
				
				try {
					FileSystem::delete($this->getDir().'/'.$fn);
				}
				catch(IOException $e){
					$this->log($e, ILogger::ERROR);
				};
			};
			
			$this->log('Backups: Other backup files deleted.');
			
			$files = [$archiveName];
		};
		
		return $files;
	} // runJobsLocked
	
	public function get(int $id): ?IBackup {
		$bak = parent::get($id);
		
		if($bak !== null){
			$bak = self::extractMainFile($bak, $this->archiveName.'.'.$this->ext, '.'.$this->ext);
		};
		
		return $bak;
	} // get
	
	public function list(): array {
		$baks = [];
		foreach(parent::list() as $id => $bak){
			$baks[$id] = self::extractMainFile($bak, $this->archiveName.'.'.$this->ext, '.'.$this->ext);
		};
		
		return $baks;
	} // list
	
	/**
	 * Tries to find the archive file in the given backup
	 *
	 * Simply returns given backup instance if it already contains a main-file.
	 * If not and it finds one, it will return new IBackup instance
	 * with such file set as main-file (and removed from the files array).
	 *
	 * @param string $s1 - prefered primary suffix to search
	 * @param string $s2 - optional secondary suffix to search
	 */
	public static function extractMainFile(IBackup $bak, string $s1, ?string $s2 = null): IBackup {
		// check if main-file already set or no files to search
		if($bak->getMainFile() !== null || count($bak->getFiles()) < 1){
			return $bak;
		};
		
		// check search strings
		if($s1 === $s2){
			$s2 = null;
		};
		
		// check for single-file backup
		$files = $bak->getFiles();
		
		if(count($files) === 1){
			$file = $files[0];
			if(
					($s1 === '' || Strings::endsWith($file->getName(), $s1))
				||
					(
							$s2 !== null
						&&
							($s2 === '' || Strings::endsWith($file->getName(), $s2))
					)
			){
				$bakNew = new Backup($bak->getId(), $bak->getTime());
				$bakNew->setMainFile($file);
				$bak = $bakNew;
			};
			
			return $bak;
		};
		
		// walk through files and extract the similar name base
		$bn = $files[0]->getName();
		
		foreach($files as $file){
			$bn = Strings::findPrefix([$bn, $file->getName()]);
		};
		
		if($bn === ''){
			// it might be better to throw InvalidStateException,
			// but this helper is declared as public static...
			return $bak;// there are completely different files in the backup
		};
		
		// remove trailing underscore (as we use it to separate job name)
		if(Strings::endsWith($bn, '_')){
			$bn = substr($bn, 0, -1);
		};
		
		// build file-names to search for
		$s1 = $bn . $s1;
		
		if($s2 !== null){
			$s2 = $bn . $s2;
		};
		
		// search through files to find files of wanted names (based on preference)
		$key = null;
		
		foreach($files as $k => $file){
			if($file->getName() === $s1){
				$key = $k;
				break;
			}
			elseif($s2 !== null && $file->getName() === $s2){
				if($key !== null){
					throw new InvalidStateException('Duplicate file-name encountered!');
				};
				
				$key = $k;
			};
		};
		
		// extract file if given and return resulting backup
		if($key !== null){
			$bakNew = new Backup($bak->getId(), $bak->getTime());
			
			$bakNew->setMainFile($files[$key]);
			unset($files[$key]);
			
			foreach($files as $file){
				$bakNew->addFile($file);
			};
			
			$bak = $bakNew;
		};
		
		return $bak;
	} // extractMainFile
} // class Archive
