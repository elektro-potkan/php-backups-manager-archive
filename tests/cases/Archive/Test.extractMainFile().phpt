<?php

declare(strict_types=1);

namespace Tests\Cases\Archive;

require __DIR__ . '/../../bootstrap.php';

use DateTimeInterface;
use Generator;
use Nette\Utils\DateTime;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\Backup;
use ElektroPotkan\Backups\IBackup;
use ElektroPotkan\Backups\IBackupFile;
use ElektroPotkan\Backups\Managers\Archive as Manager;

use Tests\Mocks\BackupFile;


class Test_extractMainFile extends Tester\TestCase {
	public static function genId(): Generator {
		for($i = 0; $i < 20; $i++){
			yield rand(-1629856847, 2629856847);
		};
	} // genId
	
	public static function genTime(int $id): Generator {
		yield DateTime::createFromFormat('U', (string) $id);
		yield DateTime::createFromFormat('U', (string) rand(-1629856847, 2629856847));
	} // genTime
	
	public static function genDir(): array {
		return [
			'/tmp/baks',
			'/var/www/tests/backups',
			'/srv/www/vhosts/w2452133/t212/baks',
		];
	} // genDir
	
	public static function genName(): array {
		return [
			'FuelApp_v1.47.2',
			'Test_v4.78.2',
			'nearly-everything-one-wants_to__have_here',
		];
	} // genName
	
	public static function genFilename(): array {
		return [
			'platform.ver',
			'readme.txt',
			'versions',
			'nejaka-databaze.sql.gz',
			'dalsi-soubory.tar',
			'reports.zip',
		];
	} // genFilename
	
	public static function genPrefix(DateTimeInterface $time): array {
		return [
			$time->format('U_Y-m-d_H-i-s'),
			$time->format('U_Y-m-d'),
			'16427485_2019-10-28_17-5',
			'some-file-name__start',
		];
	} // genPrefix
	
	/**
	 * Returns array
	 *   0 => main-file suffix for creating mock file
	 *   1 => $s1 argument for extractMainFile
	 *   2 => $s2 argument for extractMainFile
	 *   3 => use for already-set-or-no-files-or-missing test - one-by-one no-main-file case allowed
	 *   4 => use for present test - main-file-only case allowed
	 *   5 => use for present test - multiple files case allowed
	 */
	public static function genMainFileSuffix(): Generator {
		foreach(self::genMainFileSuffixMatch() as $mfs){
			yield $mfs;
		};
		
		foreach(self::genMainFileSuffixMiss() as $mfs){
			yield $mfs;
		};
	} // genMainFileSuffix
	
	public static function genMainFileSuffixMatch(): array {
		return [
			['.tar', '.tar', null, false, true, true],
			['.zip', '.zip', null, false, true, true],
			['_.tar', '.tar', '_.tar', false, true, true],
			['_.tar', '_.tar', null, true, true, true],
			['__.tar', '_.tar', null, true, true, false],
			['__.tar', '.tar', '_.tar', false, true, false],
			['__.tar', '.tar', '__.tar', false, true, true],
			['___.tar', '.tar', '__.tar', false, true, false],
			['_bak_.tar', '.tar', '_bak_.tar', false, true, false],
			['_bak_.tar', '_bak_.tar', null, true, true, true],
			['_bak_.tar', '.tar', 'bak_.tar', false, true, false],
			['_bak_.tar', '.tar', '_bak_.tar', false, true, true],
		];
	} // genMainFileSuffixMatch
	
	public static function genMainFileSuffixMiss(): array {
		return [
			['_bak_.tar', '.tar', null, false, false, true],
			['__.tar', '.tar', null, false, false, true],
			['_.tar', '.tar', null, false, false, true],
			['_main.tar', '.tar', '_bak.tar', false, false, true],
		];
	} // genMainFileSuffixMiss
	
	public static function mockFile(string $dir, string $prefix, string $name, string $suffix): IBackupFile {
		return new BackupFile($dir.'/'.$prefix.'_'.$name.$suffix, rand(0, 2**20));
	} // mockFile
	
	public static function genArgsAlreadySetOrNoFilesOrMissing(): Generator {
		foreach(self::genId() as $id){
			foreach(self::genTime($id) as $time){
				foreach(self::genDir() as $d){
					foreach(self::genName() as $n){
						foreach(self::genPrefix($time) as $p){
							foreach(self::genMainFileSuffix() as $mfs){
								$sets = [
									[true, 2],
									[true, 1],
									[true, 0],
									[false, 2],
									[false, 1],
									[false, 0],
								];
								
								foreach($sets as list($setMainFile, $setFiles)){
									if(!$setMainFile && $setFiles > 1 && !$mfs[3]){
										continue;
									};
									
									foreach(self::genFilename() as $fn){
										$bak = new Backup($id, $time);
										
										if($setMainFile){
											$bak->setMainFile(self::mockFile($d, $p, $n, $mfs[0]));
										};
										
										if($setFiles > 1){
											$bak->addFile(self::mockFile($d, $p, $n, '_'.$fn));
										}
										elseif($setFiles > 0){
											foreach(self::genFilename() as $fn){
												$bak->addFile(self::mockFile($d, $p, $n, '_'.$fn));
											};
										};
										
										yield [$bak, $mfs[1], $mfs[2]];
										
										if($setFiles < 2){
											break;
										};
									};
								};
							};
						};
					};
				};
			};
		};
	} // genArgsAlreadySetOrNoFilesOrMissing
	
	/**
	 * @dataProvider genArgsAlreadySetOrNoFilesOrMissing
	 */
	public function testAlreadySetOrNoFilesOrMissing(IBackup $bak, string $s1, ?string $s2): void {
		$bakNew = Manager::extractMainFile($bak, $s1, $s2);
		
		Assert::same($bak->getId(), $bakNew->getId());
		Assert::equal($bak->getTime(), $bakNew->getTime());
		Assert::same($bak->getMainFile(), $bakNew->getMainFile());
		Assert::equal($bak->getFiles(), $bakNew->getFiles());
		Assert::same($bak->getSize(), $bakNew->getSize());
	} // testAlreadySetOrNoFilesOrMissing
	
	public static function genArgsPresent(): Generator {
		foreach(self::genId() as $id){
			foreach(self::genTime($id) as $time){
				foreach(self::genDir() as $d){
					foreach(self::genName() as $n){
						foreach(self::genPrefix($time) as $p){
							foreach([true, false] as $setOtherFiles){
								foreach(self::genMainFileSuffixMatch() as $mfs){
									$bak = new Backup($id, $time);
									
									$files = [];
									
									$mainFile = self::mockFile($d, $p, $n, $mfs[0]);
									$bak->addFile($mainFile);
									
									if($setOtherFiles){
										if(!$mfs[5]){
											continue;// multiple files case not allowed for this suffix
										};
										
										foreach(self::genFilename() as $fn){
											$file = self::mockFile($d, $p, $n, '_'.$fn);
											$files[] = $file;
											$bak->addFile($file);
										};
									}
									elseif(!$mfs[4]){
										continue;// main-file-only case not allowed for this suffix
									};
									
									yield [$bak, $mfs[1], $mfs[2], $mainFile, $files];
								};
								
								foreach(self::genMainFileSuffixMiss() as $mfs){
									$bak = new Backup($id, $time);
									
									$files = [];
									
									$file = self::mockFile($d, $p, $n, $mfs[0]);
									$files[] = $file;
									$bak->addFile($file);
									
									if($setOtherFiles){
										if(!$mfs[5]){
											continue;// multiple files case not allowed for this suffix
										};
										
										foreach(self::genFilename() as $fn){
											$file = self::mockFile($d, $p, $n, '_'.$fn);
											$files[] = $file;
											$bak->addFile($file);
										};
									}
									elseif(!$mfs[4]){
										continue;// main-file-only case not allowed for this suffix
									};
									
									yield [$bak, $mfs[1], $mfs[2], null, $files];
								};
							};
						};
					};
				};
			};
		};
	} // genArgsPresent
	
	/**
	 * @dataProvider genArgsPresent
	 */
	public function testPresent(IBackup $bak, string $s1, ?string $s2, ?IBackupFile $mainFile, array $files): void {
		$bakNew = Manager::extractMainFile($bak, $s1, $s2);
		
		Assert::same($bak->getId(), $bakNew->getId());
		Assert::equal($bak->getTime(), $bakNew->getTime());
		Assert::same($mainFile, $bakNew->getMainFile());
		Assert::same($bak->getSize(), $bakNew->getSize());
		
		// check files arrays ignoring their keys
		Assert::count(count($files), $bakNew->getFiles());
		foreach($files as $file){
			Assert::contains($file, $bakNew->getFiles());
		};
	} // testPresent
} // class Test_extractMainFile


(new Test_extractMainFile)->run();
