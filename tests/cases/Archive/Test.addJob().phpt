<?php

declare(strict_types=1);

namespace Tests\Cases\Archive;

require __DIR__ . '/../../bootstrap.php';

use DateTimeInterface;
use Generator;
use InvalidArgumentException;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\Managers\Archive as Manager;
use ElektroPotkan\Backups\PurgeRules\AlwaysKeep;

use Tests\Mocks\Job;


class Test_addJob extends Tester\TestCase {
	/**
	 * @param string[] $excludeNames
	 * @return string[]
	 */
	public static function genFileName(array $excludeNames = []): array {
		$charsExt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$charsName = $charsExt . '_-';
		
		$maxExt = strlen($charsExt) - 1;
		$maxName = strlen($charsName) - 1;
		
		$lenExt = rand(0, 6);
		$lenName = rand(1, 20);
		
		$ext = '';
		for($i = 0; $i < $lenExt; $i++){
			$ext .= $charsExt[rand(0, $maxExt)];
		};
		
		do {
			$name = '';
			for($i = 0; $i < $lenName; $i++){
				$name .= $charsName[rand(0, $maxName)];
			};
		}
		while(in_array($name, $excludeNames, true));
		
		return [$name, $ext];
	} // genFileName
	
	/**
	 * @param string[] $excludeNames
	 * @return [string, Job]
	 */
	public static function genJob(array $excludeNames = []): array {
		list($name, $ext) = self::genFileName($excludeNames);
		return [$name, new Job($ext)];
	} // genJob
	
	/**
	 * @param string[] $excludeNames
	 */
	public static function genJobs(int $count, array $excludeNames = []): Generator {
		for($i = 0; $i < $count; $i++){
			list($name, $job) = self::genJob($excludeNames);
			yield $name => $job;
			$excludeNames[] = $name;
		};
	} // genJobs
	
	public static function mockManager(string $archiveFormat, string $archiveName): Manager {
		return new class('/tmp', 'test-name', new AlwaysKeep, $archiveFormat, $archiveName, true) extends Manager {
			protected function selectFormat(?string $format): string {
				return $format;
			}
		};
	} // mockFile
	
	public static function testColliding(): void {
		for($i = 0; $i < 20; $i++){
			foreach(['tar', 'tar.bz2', 'tar.gz', 'zip'] as $format){
				foreach([false, true] as $collidingOnly){
					$archiveName = self::genFileName()[0];
					$usedNames = [$archiveName];
					
					$manager = self::mockManager($format, '_'.$archiveName);
					
					if(!$collidingOnly){
						foreach(self::genJobs(rand(2, 10), $usedNames) as $name => $job){
							$usedNames[] = $name;
							$manager->addJob($name, $job);
						};
					};
					
					Assert::exception(
						function() use($archiveName, $format, $manager): void {
							$manager->addJob($archiveName, new Job($format));
						},
						InvalidArgumentException::class,
						'This job name cannot be used!%a?%'
					);
					
					if(!$collidingOnly){
						foreach(self::genJobs(rand(2, 10), $usedNames) as $name => $job){
							$manager->addJob($name, $job);
						};
					};
				};
			};
		};
	} // testColliding
} // class Test_addJob


(new Test_addJob)->run();
