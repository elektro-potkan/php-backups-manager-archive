<?php

declare(strict_types=1);

namespace Tests\Mocks;

use InvalidArgumentException;
use Nette;

use ElektroPotkan\Backups\IBackupFile;


/**
 * Single backup file info
 *
 * @property-read string $name
 * @property-read string $path
 * @property-read int $size
 */
class BackupFile implements IBackupFile {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $name;
	
	/** @var string */
	private $path;
	
	/** @var int */
	private $size;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $path, int $size){
		if($path === '' || substr($path, 0, 1) !== '/'){
			throw new InvalidArgumentException('Absolute path required!');
		};
		
		$name = basename($path);
		if(in_array($name, ['', '.', '..'], true)){
			throw new InvalidArgumentException('Invalid path given!');
		};
		
		if($size < 0){
			throw new InvalidArgumentException('Size cannot be negative!');
		};
		
		$this->name = $name;
		$this->path = $path;
		$this->size = $size;
	} // constructor
	
	/**
	 * Returns file-name of backup file
	 */
	public function getName(): string {
		return $this->name;
	} // getName
	
	/**
	 * Returns full path to backup file
	 */
	public function getPath(): string {
		return $this->path;
	} // getPath
	
	/**
	 * Returns size of backup file
	 */
	public function getSize(): int {
		return $this->size;
	} // getSize
} // class BackupFile
