<?php

declare(strict_types=1);

namespace Tests\Mocks;

use ElektroPotkan\Backups\IJob;


/**
 * Stub backup job
 */
class Job implements IJob {
	/** @var string */
	private $ext;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $extension){
		$this->ext = $extension;
	} // constructor
	
	function create(string $path): void {
		// stub only
	} // create
	
	function getExtension(): string {
		return $this->ext;
	} // getExtension
} // class Job
