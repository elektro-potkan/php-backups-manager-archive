Backups - Archive manager
=========================

Manager creating archive of the whole backup.


Usage
-----
To use the manager, You need at least the `phar` extension.
To support compressed tar archives, the `zlib` (for gzip) or `bzip2` extensions must also be installed.

If the `phar` extension is unavailable to You, the `zip` extension can be used as fallback
(but You will be then limited to zip format only).

If You set the format to `AUTO` (the default value if not provided),
the manager will detect the available extensions and choose the first possible format from the list:
1. `TAR_BZIP2`
2. `TAR_GZIP`
3. `TAR`
4. `ZIP`

### Example
The Archive manager just enhances the features from the main `elektro-potkan/backups` package.
Look at the example in its README first.

```php
$manager = new ElektroPotkan\Backups\Managers\Archive(
	'path-to-backups-directory',
	'MyApp-v1.47.2',
	$purgeRule,
	ElektroPotkan\Backups\Managers\Archive::ARCHIVE_FORMAT_ZIP,
	'_bak_',// archive name suffix (default empty; must not collide with the jobs' names)
	true// keep individual backup files (default false = delete them after creating the final archive)
);
```

### Backup main-file
When retrieving the existing backups using `$manager->get($id)` or `$manager->list()`,
the returned `IBackup` instances will have the `mainFile` property set to the archive file.

```php
$backup->mainFile;// the whole-backup archive as IBackupFile instance

$backup->files;// IBackupFile instances of the individual files (except the archive)
               // Empty if the files were deleted after creating the archive
               // (depends on the Archive manager configuration above).
```

The manager do not have any kind of internal storage or cache,
everything is retrieved from the filesystem on each call.
It will be very easy to get out of sync othewrwise.
Due to that, when the configuration of archive name or format changes,
the main-file of older backups might not be correctly filtered out of the individual files.
In such case, the `mainFile` property will be `null`
and the archive file will be contained in the `files` property.

### Archive name vs Job name
The jobs files are named using the same schema as in the `Simple`/`Locking` backup managers
(provided by the main `elektro-potkan/backups` package):
```
timestamp_date_name_job-name.ext
```
E.g. for manager configured with `MyApp-v1.47.2` as `$name` parameter,
backup created at `2021-08-30 20:07:24 UTC`,
job added with the name `test` and returning extension `txt`,
the resulting file-name would be:
```
1630354044_2021-08-30_MyApp-v1.47.2_test.txt
```

If the manager was configured with empty `$archiveName` (the default value)
and tar as the archive format, the resulting archive file-name would be:
```
1630354044_2021-08-30_MyApp-v1.47.2.tar
```

If it was configured with `_bak_` as archive name (like in the example above),
the resulting archive file-name would be:
```
1630354044_2021-08-30_MyApp-v1.47.2_bak_.tar
```
There is no implicit separator for the archive name
(like for the jobs, where it is always underscore and non-empty job name).

Due to this, You cannot have an archive name of `_something` with zip format
and a job of the name `something` returning `zip` as its extension.
In that case the manager will throw an `InvalidArgumentException`
when You try to add a job of such colliding name and extension.


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
